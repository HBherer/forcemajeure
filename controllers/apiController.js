const ArticleModel = require('../models/api');

module.exports = {
    getAllArticle: async (req, res, next) => {
        try {
            let document = await ArticleModel.getAllArticle()
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get all article")
        }
    },
    getArticleById: async (req, res, next) => {
        try {
            let document = await ArticleModel.getArticleById(req.params.id)
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get article by ID")
        }
    },
    getAPIByActualites: async (req, res, next) => {
        try {
            let document = await ArticleModel.getAPIByActualites()
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get article by actualites")
        }
    },
    getAPIByConseils: async (req, res, next) => {
        try {
            let document = await ArticleModel.getAPIByConseils()
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get article by Conseils")
        }
    },
    getAPIByReferencement: async (req, res, next) => {
        try {
            let document = await ArticleModel.getAPIByReferencement()
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get article by Referencement")
        }
    },
    getAPIByAnalytics: async (req, res, next) => {
        try {
            let document = await ArticleModel.getAPIByAnalytics()
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get article by Analytics")
        }
    },
    getAPIByWeb: async (req, res, next) => {
        try {
            let document = await ArticleModel.getAPIByWeb()
            res.json(document)
        } catch (err) {
            console.log(err)
            res.status(500), send("erreur interne to get article by Web")
        }
    },
};
