var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let ArticleSchema = new Schema({
    title: String,
    img: String,
    category: String,
    author: String,
    resum: String,
    content: String,
    month: String,
    datePublished: String,
}, {
    timestamps: true
});
exports.ArticleModel = Mongoose.model("Article", ArticleSchema);
