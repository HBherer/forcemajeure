var Mongoose = require("mongoose");

var Schema = Mongoose.Schema;
let TeamMateSchema = new Schema({
    name: String,
    img: String,
    userName: String,
    quote: String,
    webSite: String,
    linkedin: String,
}, {
    timestamps: true
});
exports.TeamMateModel = Mongoose.model("TeamMate", TeamMateSchema);